<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ include file="/WEB-INF/jsp/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>顾客展示页面</title>
</head>
<body>
<div style="margin-left:95px;margin-top:-43px;">
<h6>高级搜索:</h6>
<form action="customer/select" method="post">
User：<input type="text" name="user" id="user">
Tel：<input type="text" name="tel" id="tel">
<input type="submit" value="提交">
</form>
</div>
<div class="data-div">
		<div class="row tableHeader">
			<div class="col-xs-1 ">ID</div>
			<div class="col-xs-1">user</div>
			<div class="col-xs-1">firstName</div>
			<div class="col-xs-1 ">secondName</div>
			<div class="col-xs-1">address</div>
			<!-- <div class="col-xs-1">level</div> -->
			<div class="col-xs-1">vip</div>
			<div class="col-xs-2">email</div>
			<div class="col-xs-1">tel</div>
			<div class="col-xs-1">state</div>
		</div>

		<c:forEach items="${list}" var="customerlist" varStatus="vs">
			<div class="row ">
				<div class="col-xs-1 " name="id">${customerlist.id}</div>
				<div class="col-xs-1" name="thirdLevel">${customerlist.user}</div>
				<div class="col-xs-1">${customerlist.firstName}</div>
				<div class="col-xs-1 ">${customerlist.secondName}</div>
				<div class="col-xs-1">${customerlist.address}</div>
				<%-- <div class="col-xs-1">${customerlist.level}</div> --%>
				<div class="col-xs-1">${customerlist.vip}</div>
				<div class="col-xs-2">${customerlist.email}</div>
				<div class="col-xs-1 ">${customerlist.tel}</div>
				<div class="col-xs-1">${customerlist.state}</div>
				<div class="col-xs-2">
					<button class="btn btn-success btn-xs" data-toggle="modal"
						data-target="#reviseUser">详情</button>
					<button class="btn btn-danger btn-xs" data-toggle="modal"
						data-target="#deleteUser"><a href="customer/delete?id=${customerlist.id}">删除</a></button>
				</div>
			</div>
			</c:forEach>
		</div>
		 <div class="row">${page.pageStr}</div>
	</div>
</body>
</html>