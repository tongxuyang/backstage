<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ include file="/WEB-INF/jsp/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>商铺条件查询</title>
</head>
<body>
<div style="margin-left:95px;margin-top:-43px;">
<h6>高级搜索:</h6>
<form action="shop/condition" method="post">

ID:<input type="text" name="id" id="id">
SecondLevel:<input type="text" name="secondLevel" id="secondLevel">
<input type="submit" value="提交">
</form>
</div>
<div class="data-div">
		    <div class="row tableHeader">
			<div class="col-xs-1 ">ID</div>
			<div class="col-xs-1">firstLevel</div>
			<div class="col-xs-1">secondLevel</div>
			<div class="col-xs-1 ">attribute</div>
			<div class="col-xs-1">desp</div>
			<div class="col-xs-1">master</div>
			<div class="col-xs-1">state</div>
			<div class="col-xs-1">goods</div>
		</div>
		
		<c:forEach items="${list}" var="shoplist" varStatus="vs">
			<div class="row ">
				<div class="col-xs-1 " name="id">${shoplist.id}</div>
				<div class="col-xs-1" name="thirdLevel">${shoplist.firstLevel}</div>
				<div class="col-xs-1">${shoplist.secondLevel}</div>
				<div class="col-xs-1 ">${shoplist.attribute}</div>
				<div class="col-xs-1">${shoplist.desp}</div>
				<div class="col-xs-1">${shoplist.master}</div>
				<div class="col-xs-1">${shoplist.state}</div>
				<div class="col-xs-1">${shoplist.state}</div>
				<div class="col-xs-1 ">${shoplist.goods}</div>
				<div class="col-xs-2">
					<button class="btn btn-success btn-xs" data-toggle="modal"
						data-target="#reviseUser">详情</button>
					<button class="btn btn-danger btn-xs" data-toggle="modal"
						data-target="#deleteUser"><a href="shop/delete?id=${shoplist.id}">删除</a></button>
				</div>
			</div>
			</c:forEach>
							<div class="modal fade" id="deleteChar" role="dialog"
							aria-labelledby="gridSystemModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="gridSystemModalLabel">提示</h4>
									</div>
									<div class="modal-body">
										<div class="container-fluid">确定要删除该权限？删除后不可恢复！</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-xs btn-white"
											data-dismiss="modal">取 消</button>
										<button type="button" class="btn btn-xs btn-danger">删除</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
		
			
		</div>
		 <div class="row">${page.pageStr}</div>
	</div>
	
</body>
</html>