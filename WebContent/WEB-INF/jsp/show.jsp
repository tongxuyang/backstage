<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href=" ${pageContext.request.contextPath}/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="css/slide.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/flat-ui.min.css" /> 
<link rel="stylesheet" type="text/css" href="css/jquery.nouislider.css">
</head>
<body>
	<div class="data-div">
		<div class="row tableHeader">
			<div class="col-xs-1 ">ID</div>
			<div class="col-xs-1">thirdLevel</div>
			<div class="col-xs-1">attribute</div>
			<div class="col-xs-1 ">num</div>
			<div class="col-xs-1">price</div>
			<div class="col-xs-1">time</div>
			<div class="col-xs-1">state</div>
			<div class="col-xs-1">shopId</div>
			<div class="col-xs-1">hot</div>
		</div>

		<c:forEach items="${goodsallinfo}" var="goodsinfo" varStatus="vs">
			<div class="row ">
				<div class="col-xs-1 " name="id">${goodsinfo.id}</div>
				<div class="col-xs-1" name="thirdLevel">${goodsinfo.thirdLevel}</div>
				<div class="col-xs-1">part</div>
				<div class="col-xs-1 ">1102</div>
				<div class="col-xs-1">100</div>
				<div class="col-xs-1">65$</div>
				<div class="col-xs-1">on</div>
				<div class="col-xs-1">10001</div>
				<div class="col-xs-1 ">10001</div>

				<div class="col-xs-2">
					<button class="btn btn-success btn-xs" data-toggle="modal"
						data-target="#reviseUser">详情</button>
					<button class="btn btn-danger btn-xs" data-toggle="modal"
						data-target="#deleteUser">删除</button>
				</div>
			</div>
			</c:forEach>
		</div>
		 <div class="row">${page.pageStr}</div>
	</div>
	<!--弹出框-->
	<!-- <div class="modal fade" id="reviseUser" role="dialog" aria-labelledby="gridSystemModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="gridSystemModalLabel">修改用户</h4>
</div>
<div class="modal-body">
<div class="container-fluid">
<form class="form-horizontal">
<div class="form-group ">
<label for="sName" class="col-xs-3 control-label">用户名：</label>
<div class="col-xs-8 ">
<input type="email" class="form-control input-sm duiqi" id="sName" placeholder="">
</div>
</div>
<div class="form-group">
<label for="sLink" class="col-xs-3 control-label">真实姓名：</label>
<div class="col-xs-8 ">
<input type="" class="form-control input-sm duiqi" id="sLink" placeholder="">
</div>
</div>
<div class="form-group">
<label for="sOrd" class="col-xs-3 control-label">电子邮箱：</label>
<div class="col-xs-8">
<input type="" class="form-control input-sm duiqi" id="sOrd" placeholder="">
</div>
</div>
<div class="form-group">
<label for="sKnot" class="col-xs-3 control-label">电话：</label>
<div class="col-xs-8">
<input type="" class="form-control input-sm duiqi" id="sKnot" placeholder="">
</div>
</div>
<div class="form-group">
<label for="sKnot" class="col-xs-3 control-label">地区：</label>
<div class="col-xs-8">
<input type="" class="form-control input-sm duiqi" id="sKnot" placeholder="">
</div>
</div>
<div class="form-group">
<label for="sKnot" class="col-xs-3 control-label">权限：</label>
<div class="col-xs-8">
<input type="" class="form-control input-sm duiqi" id="sKnot" placeholder="">
</div>
</div>
<div class="form-group">
<label for="situation" class="col-xs-3 control-label">状态：</label>
<div class="col-xs-8">
<label class="control-label" for="anniu">
<input type="radio" name="situation" id="normal">正常</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label class="control-label" for="meun">
<input type="radio" name="situation" id="forbid"> 禁用</label>
</div>
</div>
</form>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
<button type="button" class="btn btn-xs btn-green">保 存</button>
</div>
</div>
								/.modal-content
</div>
							/.modal-dialog -->

</body>
</html>