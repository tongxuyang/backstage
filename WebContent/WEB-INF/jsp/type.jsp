<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href=" ${pageContext.request.contextPath}/">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>分类管理</title>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="css/slide.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/flat-ui.min.css" /> 
<link rel="stylesheet" type="text/css" href="css/jquery.nouislider.css">
</head>
<body>

<div style="margin-left:95px;margin-top:-43px;">
<h6>高级搜索:</h6>
<form action="itemList/type" method="post">

ID:<input type="text" name="id" id="id">
ThirdLevel:<input type="text" name="thirdLevel" id="thirdLevel">
<input type="submit" value="提交">
</form>
</div>
<div class="data-div">
		    <div class="row tableHeader">
			<div class="col-xs-1 ">ID</div>
			<div class="col-xs-1">thirdLevel</div>
			<div class="col-xs-1">attribute</div>
			<div class="col-xs-1 ">num</div>
			<div class="col-xs-1">price</div>
			<div class="col-xs-1">time</div>
			<div class="col-xs-1">state</div>
			<div class="col-xs-1">shopId</div>
			<div class="col-xs-1">hot</div>
		</div>
		
		<c:forEach items="${list}" var="goodsinfo" varStatus="vs">
			<div class="row">
			<form action="itemList/delete?id=${goodsinfo.id}" method="post">
				<div class="col-xs-1 " name="id">${goodsinfo.id}</div>
				<div class="col-xs-1" name="thirdLevel">${goodsinfo.thirdLevel}</div>
				<div class="col-xs-1">${goodsinfo.attribute}</div>
				<div class="col-xs-1 ">${goodsinfo.num}</div>
				<div class="col-xs-1">${goodsinfo.price}</div>
				<div class="col-xs-1">${goodsinfo.time}</div>
				<div class="col-xs-1">${goodsinfo.state}</div>
				<div class="col-xs-1">${goodsinfo.shopId}</div>
				<div class="col-xs-1 ">${goodsinfo.hot}</div>
				<div class="col-xs-2">
					<button class="btn btn-success btn-xs" data-toggle="modal"
						data-target="#reviseUser">详情</button>
					<button class="btn btn-danger btn-xs" data-toggle="modal"
						data-target="#deleteChar">删除</button>
			
				</div>
							<div class="modal fade" id="deleteChar" role="dialog"
							aria-labelledby="gridSystemModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="gridSystemModalLabel">提示</h4>
									</div>
									<div class="modal-body">
										<div class="container-fluid">确定要删除该权限？删除后不可恢复！</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-xs btn-white"
											data-dismiss="modal">取 消</button>
										<button type="button" class="btn btn-xs btn-danger">删除</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
			</div>
			</form>
			</c:forEach>
			
		</div>
		 <div class="row">${page.pageStr}</div>
	</div>
	
</body>
<script>
$(function (){
    $("#btn").click(function (){
    	var id=$("#id").val();
    	var thirdLevel=$("#thirdLevel").val();
    	var trs;
    	var tbody="";
    	alert(thirdLevel);
        $.ajax({
            type:"post",
            url:"itemList/type",
            dataType:"json",    //data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
      		data:{id:id,thirdLevel:thirdLevel},
            success:function (data) {
            	alert(data)
            /* 	$.each(data.list,function(n,value){
            		trs="<div class='col-xs-1 '>"+value.id+"</div>"+
            		"<div class='col-xs-1 '>"+value.thirdLevel+"</div>"+
            		"<div class='col-xs-1 '>"+value.attribute+"</div>"+
            		"<div class='col-xs-1 '>"+value.num+"</div>"+
            		"<div class='col-xs-1 '>"+value.state+"</div>"+
            		"<div class='col-xs-1 '>"+value.state+"</div>"
            		 tbody+= trs; 
            	})
            	$(".tablebody").append(tbody); */
        }
        })
    })
})

</script>
</html>