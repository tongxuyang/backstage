/**
 * 注册页面的JS
 */
window.onload = function() {
		var xmlhttp;
		if (window.XMLHttpRequest) {

			xmlhttp = new XMLHttpRequest();

		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}
		var username = document.getElementById("username");//获取input框中的内容

		usernickname.onblur = function() {
			xmlhttp.open("POST", "RegisterAjax", true);
			xmlhttp.setRequestHeader("Content-type",
					"application/x-www-form-urlencoded");
			xmlhttp.send("username=" + usernickname.value);
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					var status = xmlhttp.responseText;
					if (status == 1) {

						document.getElementById("span1").innerHTML = "* 用户名已经存在";

					} else {

						document.getElementById("span1").innerHTML = " ";

					}

				}
			}

		}
	}
	//判断用户名不能为空
	function myid() {
		var username = document.getElementById("username")
		var lab1 = document.getElementById("span1")
		var reg = /^[0-9a-zA-Z]{6,30}$/;
		if (username.value == "" || username.value == null) {
			lab1.innerHTML = "* 用户名不能为空";
		} else {
			if (!reg.test(username.value)) {
				lab1.innerHTML = "* 用户名格式有误"
			}else{
				lab1.innerHTML = ""
			}
		}
	}
	//判断密码不能为空
	function mypassword() {
		var password = document.getElementById("password")
		var lab2 = document.getElementById("passwordspan")
		var reg = /^[0-9a-zA-Z]{6,20}$/;
		if (password.value == "" || password.value ==null) {
			lab2.innerHTML = "* 密码不能为空"
		} else {
			if (!reg.test(password.value)) {
				lab2.innerHTML = "* 密码格式有误，只能输入数字字母6-20位"
			} else {
				lab2.innerHTML = " "
			}
		}
	}
	//验证手机号码
	function mytel(){
var mytel=document.getElementById("usertelephone")
 var lab5=document.getElementById("telspan")
  var reg= /^[1][358]\d{9}$/;
    if(reg.test(mytel.value)){
  lab5.innerHTML=""
}else {
 lab5.innerHTML="*手机号码格式有误"

 }
}