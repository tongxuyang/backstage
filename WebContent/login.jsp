<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${pageContext.request.contextPath}/">
<script src="js/jquery.min.js"></script>
 <script type="text/javascript" src="js/Register.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>backstage manager system</title>
<link rel="stylesheet" href="common/layui/css/layui.css">
<link rel="stylesheet" href="common/css/sccl.css">
</head>
<body class="login-bg">
    <div class="login-box">
        <header>
            <h1>闪电网后台管理系统</h1>
        </header>
        <div class="login-main">
		<!-- <form action="login/main" class="layui-form" method="post">  -->
				<input name="__RequestVerificationToken" type="hidden" value="">                
				<div class="layui-form-item">
					<span	id="span1" style="font-size: 4px; color: red" ></span>
					<input type="text" name="username" id="username" lay-verify="userName" autocomplete="off" placeholder="这里输入登录名" class="layui-input" onblur="myid()">
				</div>
				<div class="layui-form-item">
					<span id="passwordspan" style="font-size: 4px; color: red" ></span>
					<input type="password" name="password" id="password" lay-verify="password" autocomplete="off" placeholder="这里输入密码" class="layui-input" onblur="mypassword()">
				</div>
				<div class="layui-form-item">
					<div class="pull-left login-remember">
						<label>记住帐号？</label>

						<input type="checkbox" name="rememberMe" value="true" lay-skin="switch" title="记住帐号"><div class="layui-unselect layui-form-switch"><i></i></div>
					</div>
					<div class="pull-right">
						 <button class="layui-btn layui-btn-primary" lay-submit="login/main" lay-filter="login" id="btn" onclick="submit()">
							 登录
						</button> 
					
					</div>
					<div class="clear"></div>
				</div>
			<!-- </form>    -->    
		</div>
        <footer>
            <p>xuan © www.ouhua.com</p>
        </footer>
    </div>
    <script>
 /*   function objtest(){
        $("#btn").click(function (){
        	var username=$("#username").val();
        	var password=$("#password").val();
        	
        	alert(thirdLevel);
            $.ajax({
                type:"post",
                url:"login/main",
                dataType:"json",
                cache: false,//data传递的是一个json类型的值，而不是字符串，且必须标明dataType的类型，否则会出现400错误或者其他错误。
          		data:{username:username,password:password},
                success:function (data) {
                alert(data)
             
            }
            })
        })
    }
   
    */
   
   function submit(){
   	/* if(check()){ */
   		var username=$("#username").val();
        var password=$("#password").val();
   		$("#btn").html("登录中...");
   		$.ajax({
   			type: "post",
   			url: 'login/main',
   	    	data: { username:username, password:password},
   			dataType:'json',
   			cache: false,
   			success: function(data){
   				
   				if("success" == data.result){
   					
   					alert("登陆成功")
   				
   					window.location.href= "login/tomain"; 
   				}else if("usererror" ==data.result){
   			    		   alert("用户名或密码错误")
   			    	window.location.href= "login.jsp"; 
   					$("#btn").html("请重新登录");
   			    		
   				}
   			}
   	    
   		});
   	/* } */
   }
    </script>
    <script type="text/html" id="code-temp">
        <div class="login-code-box">
            <input type="text" class="layui-input" id="code" />
            <img id="valiCode" src="/manage/validatecode?v=636150612041789540" alt="验证码" />
        </div>
    </script>
    <script src="../common/layui/layui.js"></script>
    <script>
    
        layui.use(['layer', 'form'], function () {
            var layer = layui.layer,
				$ = layui.jquery,
				form = layui.form();

            form.verify({
                userName: function (value) {
                    if (value === '')
                        return '请输入用户名';
                },
                password: function (value) {
                    if (value === '')
                        return '请输入密码';
                }
            });

            var errorCount = 0;

            form.on('submit(login)', function (data) {
				window.location.href = "..common/page/index.html";
                /*if (errorCount > 5) {
                    layer.open({
                        title: '<img src="' + location.origin + '/Plugins/layui/images/face/7.gif" alt="[害羞]">输入验证码',
                        type: 1,
                        content: document.getElementById('code-temp').innerHTML,
                        btn: ['确定'],
                        yes: function (index, layero) {
                            var $code = $('#code');
                            if ($code.val() === '') {
                                layer.msg('输入验证码啦，让我知道你是人类。');
                                isCheck = false;
                            } else {
                                $('input[name=verifyCode]').val();
                                var params = data.field;
                                params.verifyCode = $code.val();
                                submit($,params);
                                layer.close(index);
                            }
                        },
                        area: ['250px', '150px']
                    });
                    $('#valiCode').off('click').on('click', function () {
                        this.src = '/manage/validatecode?v=' + new Date().getTime();
                    });
                }else{
                    submit($,data.field);
                }

                return false;*/
            });

        });
      

        /*function submit($,params){
            $.post('/manage/login',params , function (res) {
                if (!res.success) {
                    if (res.data !== undefined)
                        errorCount = res.data.errorCount
                    layer.msg(res.message,{icon:2});
                }else
                {
                    layer.msg(res.message,{icon:1},function(index){
                        layer.close(index);
                        location.href='/manage';
                    });
                }
            }, 'json');
        }*/
    </script>
  </body>
</html>