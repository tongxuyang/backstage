package com.ouhua.ssm.base;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;


import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ouhua.ssm.entity.Page;

import com.ouhua.ssm.utils.FileServer;
import com.ouhua.ssm.utils.FileServer.FileTypeEnum;
import com.ouhua.ssm.utils.Logger;
import com.ouhua.ssm.utils.PageData;
import com.ouhua.ssm.utils.Tools;
import com.ouhua.ssm.utils.UuidUtil;


public class BaseController {
	
	protected Logger logger = Logger.getLogger(this.getClass());

	public PageData pageData;
	/**
	 * 得到PageData
	 */
	public PageData getPageData(){
		return new PageData(this.getRequest());
	}
	
	/*public PageData getMultiPageData(){
		return new PageData(this.getMultiRequest());
	}*/
	
	/**
	 * 设置PageData
	 * @param pd
	 */
	public void setPageData(PageData pd){
		pageData=pd;
	}
	/**
	 * 得到ModelAndView
	 */
	public ModelAndView getModelAndView(){
		return new ModelAndView();
	}
	
	/**
	 * 得到request对象
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		return request;
	}
    
	/**
	 * 得到Multirequest对象
	 * @return
	 */
	/*public MultipartHttpServletRequest getMultiRequest(){
		MultipartHttpServletRequest multiRequest= (MultipartHttpServletRequest)((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		return multiRequest;
	}*/
	/**
	 * 得到32位的uuid
	 * @return
	 */
	public String get32UUID(){
		return UuidUtil.get32UUID();
	}
	
	/**
	 * 得到分页列表的信�? 
	 */
	public Page getPage(){
		return new Page();
	}
	
	/**
	 * 日志�?�?
	 */
	public static void logBefore(Logger logger, String interfaceName){
		logger.info("");
		logger.info("start");
		logger.info(interfaceName);
	}
	
	/**
	 * 日志结束
	 */
	public static void logAfter(Logger logger){
		logger.info("end");
		logger.info("");
	}
	
	/** 文件处理 **/
	public String saveFileList(List<MultipartFile> tempFileList, FileTypeEnum fileTypeEnum){
		return saveFileList(tempFileList.toArray(new MultipartFile[tempFileList.size()]), fileTypeEnum);
	}
	
	/**
	 * 向服务器端保存多个文�?
	 * @param MultipartFile[]
	 * @param FileTypeEnum
	 * */
	public String saveFileList(MultipartFile[] tempFileList, FileTypeEnum fileTypeEnum){
		StringBuffer relativePaths = new StringBuffer();
		if(tempFileList != null && tempFileList.length>0){
			for(MultipartFile tempFile: tempFileList){
				String relativePath= saveFile(tempFile, fileTypeEnum);
				if(relativePath != null){
					if(!"".equals(relativePaths.toString())){
						relativePaths.append(",");
					}
					relativePaths.append(relativePath);
				}
			}
		}
		if(!"".equals(relativePaths.toString())){
			return relativePaths.toString();
		}else{
			return null;
		}
	}
	/**
	 * 向服务器端保存文�?
	 * @param MultipartFile
	 * @param FileTypeEnum
	 * */
	public String saveFile(MultipartFile tempFile, FileTypeEnum fileTypeEnum){
		String relativePath = null;
		if(tempFile != null && !tempFile.isEmpty()){
			try {
				//String fileName = tempFile.getOriginalFilename();
				//将文件保存到文件服务�?
				relativePath= FileServer.saveFile(tempFile, fileTypeEnum);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return relativePath;
	}
	

	/**
	 * 删除服务器端的文�?
	 * @param filePath  数据库文件的路径
	 * */
	public void delFileOnServer(String filePath) throws Exception{
		if (Tools.notEmpty(filePath)) {
			File file = new File(FileServer.writeAddress + filePath);
			if(file.exists()){
				file.delete();
			}
		}
	}
	
	/**
	 * 判断是否是手机浏�?
	 * @param ServletRequest  
	 * */
	public static boolean isMicroMessengerBrowser(ServletRequest request){
		boolean isMicroMessengerBrowser = false;
		if(((HttpServletRequest) request).getHeader("user-agent")!=null){
			String ua = ((HttpServletRequest) request).getHeader("user-agent").toLowerCase();
			String userAgent = request.getParameter("user-agent");
			if(Tools.notEmpty(userAgent)){
				ua = userAgent;
			}
			String[] keywords = {"micromessenger", "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser"};
			//String[] keywords = {"micromessenger", "Windows Phone"};
			for (String keyword: keywords) {
				if(ua.contains(keyword.toLowerCase())){
					isMicroMessengerBrowser = true;
					break;
				}
			}
		}
		return isMicroMessengerBrowser;
	}
/*	*//**
	 * 获取用户对象
	 * @return
	 * @throws Exception
	 *//*
	public User getUser() throws Exception{
		Subject currentUser = SecurityUtils.getSubject();  
		Session session = currentUser.getSession();
		User user=(User)session.getAttribute(Const.SESSION_USER);
		if(user==null){
			return null;
		}
		return user;
	}
	*/
	/**
	 * 添加用户参数
	 * @param USER_ID  用户id
	 * @param ROLE_ID  用户角色
	 * @throws Exception 
	 * */
/*	public PageData setParams(PageData pd) throws Exception{
		User user = getUser();
		if(user != null){
			pd.put("ROLE_ID", user.getROLE_ID());
			pd.put("USER_ID", user.getUSER_ID());
		}
		return pd;
	}
*/
	/**
	 * 判断当前用户是admin
	 * */
	/*public boolean isAdmin(User user) throws Exception{
		return (boolean) (user.getROLE_ID() != null ? "5" : false);
	}*/
}
