package com.ouhua.ssm.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ouhua.ssm.base.BaseController;
import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.service.CustomerListService;
import com.ouhua.ssm.utils.PageData;

/**
 * 顾客详细信息Controller
 * @author Administrator
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("customer")
public class CustomerListController extends BaseController{
@Autowired
private CustomerListService customerListService;
Map<String,Object> map = new LinkedHashMap<String,Object>();
/*查询客户详细信息*/
@RequestMapping("select")
	public ModelAndView selectCustomer(Page page) throws Exception{
	ModelAndView model=new ModelAndView();
	PageData pageData=this.getPageData();
	page.setPd(pageData);
	List<CustomerList> list=customerListService.getCustomerLists(page);
	model.addObject("list", list);
	model.setViewName("customershow");
	return model;
	
	}
/*删除顾客信息*/
@RequestMapping("delete")
public ModelAndView deleteCustomer(Page page){
	ModelAndView model=new ModelAndView();
	PageData pageData=this.getPageData();
	page.setPd(pageData);
	customerListService.deleteCustomerInfoById(page);
	model.setView(new RedirectView("select"));
	return model;

}
/*修改客户详细信息*/
@RequestMapping("update")
@ResponseBody
public Map<String, Object> updatecustomer(CustomerList customerList){
	if ("".equals(customerList)) {
		map.put("result", "0");
		return map;
	}
	customerListService.updateCustomerLists(customerList);
	map.put("result", "1");
	return map;
}
/*创建客户详细信息*/
@RequestMapping("create")
@ResponseBody
public Map<String, Object>  createCustomerDetailInfo(CustomerList customerList){
	if ("".equals(customerList)){
		map.put("result", "0");
		return map;
	}
	customerListService.createCustomerDetailInfo(customerList);
	map.put("result", "1");
	return map;

}

}
