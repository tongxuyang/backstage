package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ouhua.ssm.base.BaseController;
import com.ouhua.ssm.entity.ItemList;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.service.ItemListService;
import com.ouhua.ssm.utils.PageData;



/**
 * 商品详情Controller
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("itemList")
public class ItemListController extends BaseController{
@Autowired
private ItemListService itemListService;


/* 拿取商品所有信息。主要用于商品的详细信息展示页。 */
@RequestMapping("detail")
public ModelAndView selectGoodsDetailInfo(Page page) throws Exception{
ModelAndView model=new ModelAndView();
PageData pageData=this.getPageData();
	page.setPd(pageData);
	List<PageData> list=itemListService.getGoodsDetailInfo(page);
	model.addObject("goodsallinfo", list);
	model.setViewName("show");
	return model;
}
@RequestMapping("type")
public ModelAndView toTypePage(Page page){
	ModelAndView model=new ModelAndView();
	PageData pageData=this.getPageData();
	pageData.put("thirdLevel", pageData.get("thirdLevel"));
	page.setPd(pageData);
	System.out.println(page);
	List<ItemList> list=itemListService.selectGoodsDetailInfoByType(page);
	for (ItemList itemList : list) {
		System.out.println(itemList.getThirdLevel());
	}
	model.addObject("list", list);
	model.setViewName("type");
	return model;
	
		
}
/*按条件查询商品详细信息 */
@RequestMapping("totype")
public ModelAndView selectGoodsInfoByType(Page page){
	
	ModelAndView model=new ModelAndView();
	PageData pageData=this.getPageData();
	
	pageData.put("thirdLevel", pageData.get("thirdLevel"));
	page.setPd(pageData);
	System.out.println(page);
	List<ItemList> list=itemListService.selectGoodsDetailInfoByType(page);
	for (ItemList itemList : list) {
		System.out.println(itemList.getThirdLevel());
	}
	model.addObject("list", list);
	model.setViewName("type");
	return model;
}
/*通过ID删除商品详细信息*/
@RequestMapping("delete")
public ModelAndView deleteGoodsInfoById(Page page){
	ModelAndView model=new ModelAndView();
	PageData pageData=this.getPageData();
	page.setPd(pageData);
	itemListService.deleteGoodsDetailInfoById(page);
	model.setView(new RedirectView("type"));
	return model;
}
}
