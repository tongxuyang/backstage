package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ouhua.ssm.base.BaseController;
import com.ouhua.ssm.entity.User;
import com.ouhua.ssm.service.UserService;
import com.ouhua.ssm.utils.PageData;


/**
 * 登陆controller
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("login")
public class LoginController extends BaseController{
	@Autowired
	private UserService userService;
	
	@RequestMapping("main")
	@ResponseBody
	public Map<String, String> tomain(User user){
		
		Map<String, String> map=new HashMap<String, String>();
		
		PageData pageData=this.getPageData();
		user.setUsername(pageData.getString("username"));
	    user.setPassword(pageData.getString("password"));
		
		User newuser=userService.getUserInfoService(user);
			if (newuser!=null) {
				System.out.println("成功");
				map.put("result", "success");
				return map;
			}else {
				System.out.println("失败");
				map.put("result", "usererror");
				return map;
			}
				

	}
	@RequestMapping("tomain")
	public String tomainpage(){
		return "main";
			
	}
	/*退出系统*/
	@RequestMapping("exit")
	public String tologinpage(){
		return "login";
			
	}
}
