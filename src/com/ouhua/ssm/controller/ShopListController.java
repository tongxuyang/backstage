package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ouhua.ssm.base.BaseController;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.entity.ShopList;
import com.ouhua.ssm.service.ShopInfoService;
import com.ouhua.ssm.utils.PageData;
/**
 * 商铺信息controller
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("shop")
public class ShopListController extends BaseController{

	
	@Autowired
	private ShopInfoService shopInfoService;
	
	/*创建商铺信息 */
	@RequestMapping("create")
	@ResponseBody
	public Map<String, Object> createShopInfo(ShopList shopList){
		Map<String, Object> map = new HashMap<String, Object>();
		if (shopList!=null) {
			shopInfoService.createShopInfoService(shopList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}
	
	/*更新商铺信息*/
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> updateShopInfo(ShopList shopList){
	Map<String, Object> map = new HashMap<String, Object>();
	if (shopList!=null) {
		shopInfoService.updateShopInfoService(shopList);
		map.put("result", "1");
		return map;
	}
	map.put("result", "0");
	return map;
	}
	/*删除商铺信息*/
	@RequestMapping("delete")
	public ModelAndView deleteShopInfo(Page page){
		ModelAndView model=new ModelAndView();
		PageData pageData=this.getPageData();
		page.setPd(pageData);
			shopInfoService.deleteShopInfoService(page);
			model.setView(new RedirectView("condition"));
			return model;
		}
	/*按条件查询商铺信息*/
	@RequestMapping("condition")
	public ModelAndView findShopAllInfoByContion(Page page){
		ModelAndView model=new ModelAndView();
		PageData pageData=this.getPageData();
		System.out.println(pageData.get("secondLevel"));
		page.setPd(pageData);
		List<ShopList> list=shopInfoService.selectShopInfoByCondition(page);
		model.addObject("list", list);
		model.setViewName("shopcondition");
		return model;

	}
		
	}
	
	

