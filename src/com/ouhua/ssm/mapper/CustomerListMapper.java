package com.ouhua.ssm.mapper;

import java.util.List;

import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.entity.Page;




/**
 * CustomerList
 * @author Administrator
 *
 */
public interface CustomerListMapper {
	public List<CustomerList> selectCustomerListByIdlistPage(Page page);
	public void updateCustomerListById(CustomerList customerList);
	public void createCustomerDetailInfo(CustomerList customerList);
	public void deleteCustomerById(Page page);
}
