package com.ouhua.ssm.mapper;

import java.util.List;
import java.util.Map;

import com.ouhua.ssm.entity.ItemList;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.utils.PageData;


/**
 * 
 * @author tongxuyang
 *
 */
public interface ItemListMapper {

	public List<ItemList> getGoodsBasicInfoByPage(Map<String,Object> map);
	public List<PageData> getGoodsDetailInfolistPage(Page page);
	public List<ItemList> selectGoodsDetailInfoByTypelistPage(Page page);
	public void deleteGoodsDetailInfoById(Page page);
}
