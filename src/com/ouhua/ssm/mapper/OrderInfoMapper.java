package com.ouhua.ssm.mapper;

import java.util.List;

import com.ouhua.ssm.entity.OrderList;

public interface OrderInfoMapper {
public void createOrderInfo(OrderList orderList);
public void updateOrderInfo(OrderList orderList);
public List<OrderList> selectOrderInfoById(Integer id);
}
