package com.ouhua.ssm.mapper;

import java.util.List;

import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.entity.ShopList;

public interface ShopInfoMapper {

	public void createShopInfo(ShopList shopList);
	public void updateShopInfo(ShopList shopList);
	public List<ShopList> selectShopByConditionlistPage(Page page);
	public void deleteShopInfo(Page page);
}
