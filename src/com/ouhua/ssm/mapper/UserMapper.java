package com.ouhua.ssm.mapper;

import com.ouhua.ssm.entity.User;

public interface UserMapper {

	public User getUserInfo(User user);
}
