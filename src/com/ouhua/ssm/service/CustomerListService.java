package com.ouhua.ssm.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.mapper.CustomerListMapper;

/**
 * 顾客详细信息service
 * @author tongxuyang
 *
 */
@Service
public class CustomerListService {
    private CustomerListMapper customerListMapper;
	public void setCustomerListMapper(CustomerListMapper customerListMapper) {
	this.customerListMapper = customerListMapper;
}

	//查询客户详细信息
	public List<CustomerList> getCustomerLists(Page page) throws Exception{
		
		return customerListMapper.selectCustomerListByIdlistPage(page);
		
	}
	//修改客户详细信息
	public void updateCustomerLists(CustomerList customerList){
		customerListMapper.updateCustomerListById(customerList);
			
	}
	//创建客户详细信息
	public void createCustomerDetailInfo(CustomerList customerList){
		customerListMapper.createCustomerDetailInfo(customerList);
		
	}
	//删除客户信息
	public void deleteCustomerInfoById(Page page){
		customerListMapper.deleteCustomerById(page);	
	}
	
}
