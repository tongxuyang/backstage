package com.ouhua.ssm.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.stereotype.Service;
import com.ouhua.ssm.entity.ItemList;
import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.mapper.ItemListMapper;
import com.ouhua.ssm.utils.PageData;


/**
 * 商品信息Service
 * @author tongxuyang
 *
 */
@Service
public class ItemListService {

	private ItemListMapper itemListMapper;
	public void setItemListMapper(ItemListMapper itemListMapper) {
		this.itemListMapper = itemListMapper;
	}


	/* 拿取商品所有信息。主要用于商品的详细信息展示页。 */
	public List<PageData> getGoodsDetailInfo(Page page) throws Exception{
		return itemListMapper.getGoodsDetailInfolistPage(page);
	}
	/*按条件查询商品详细信息 */
	public List<ItemList> selectGoodsDetailInfoByType(Page page){
		return itemListMapper.selectGoodsDetailInfoByTypelistPage(page);
	}
/*通过ID删除商品详细信息*/
	public void deleteGoodsDetailInfoById(Page page){
		itemListMapper.deleteGoodsDetailInfoById(page);
	
	}
}
