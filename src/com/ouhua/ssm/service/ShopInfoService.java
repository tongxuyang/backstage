package com.ouhua.ssm.service;

import java.util.List;

import com.ouhua.ssm.entity.Page;
import com.ouhua.ssm.entity.ShopList;
import com.ouhua.ssm.mapper.ShopInfoMapper;
import com.ouhua.ssm.utils.PageData;


/**
 * 商铺信息service
 * @author tongxuyang
 *
 */
public class ShopInfoService {

	private ShopInfoMapper shopInfoMapper;

	public void setShopInfoMapper(ShopInfoMapper shopInfoMapper) {
		this.shopInfoMapper = shopInfoMapper;
	}
	//创建商铺信息 
	public void createShopInfoService(ShopList shopList){
		shopInfoMapper.createShopInfo(shopList);
	}
	
	//更新商铺信息
	public void updateShopInfoService(ShopList shopList){
		shopInfoMapper.updateShopInfo(shopList);
	}
	//删除商铺信息
	public void deleteShopInfoService(Page page){
		shopInfoMapper.deleteShopInfo(page);
	}
	
	//条件查询商铺信息
	public List<ShopList> selectShopInfoByCondition(Page page){
		return shopInfoMapper.selectShopByConditionlistPage(page);
		}
}
