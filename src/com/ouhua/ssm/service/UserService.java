package com.ouhua.ssm.service;

import com.ouhua.ssm.entity.User;
import com.ouhua.ssm.mapper.UserMapper;

public class UserService {

	private UserMapper userMapper;

	public void setUserMapper(UserMapper userMapper) {
		this.userMapper = userMapper;
	}
	
	public User getUserInfoService(User user){
		
		return userMapper.getUserInfo(user);
	}
}
