package com.ouhua.ssm.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");

	private final static SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");

	private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");

	private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");

	private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final static SimpleDateFormat sdfTimehm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * 获取YYYY格式
	 * 
	 * @return
	 */
	public static String getYear() {
		return sdfYear.format(new Date());
	}

	/**
	 * 获取MM格式
	 * 
	 * @return
	 */
	public static String getMonth() {
		return sdfMonth.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD格式
	 * 
	 * @return
	 */
	public static String getDay() {
		return sdfDay.format(new Date());
	}

	/**
	 * 获取YYYYMMDD格式
	 * 
	 * @return
	 */
	public static String getDays() {
		return sdfDays.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD HH:mm:ss格式
	 * 
	 * @return
	 */
	public static String getTime() {
		return sdfTime.format(new Date());
	}

	/**
	 * 获取YYYY-MM-DD HH:mm格式
	 * 
	 * @return
	 */
	public static String getTimehm() {
		return sdfTimehm.format(new Date());
	}

	public static String getTimeh() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		return format.format(new Date()) + ":00";
	}

	/**
	 * @Title: compareDate
	 * @Description: TODO(日期比较，如果s>=e 返回true 否则返回false)
	 * @param s
	 * @param e
	 * @return boolean
	 * @throws @author
	 *             luguosui
	 */
	public static boolean compareDate(String s, String e) {
		if (fomatDate(s) == null || fomatDate(e) == null) {
			return false;
		}
		return fomatDate(s).getTime() >= fomatDate(e).getTime();
	}

	/**
	 * 格式化日�?
	 * 
	 * @return
	 */
	public static Date fomatDate(String date) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 格式化日�?
	 * 
	 * @return
	 */
	public static Date toFomatDate(String date) {
		DateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
		try {
			return fmt.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 格式化日�? yyyyMMdd---- yyyy-MM-dd
	 * 
	 * @return
	 */
	public static String formatStringDate(String str) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		return format1.format(date);
	}

	/**
	 * 校验日期是否合法
	 * 
	 * @return
	 */
	public static boolean isValidDate(String s) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fmt.parse(s);
			return true;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或�?�NullPointerException，就说明格式不对
			return false;
		}
	}

	public static int getDiffYear(String startTime, String endTime) {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		try {
			int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(startTime).getTime()) / (1000 * 60 * 60 * 24))
					/ 365);
			return years;
		} catch (Exception e) {
			// 如果throw java.text.ParseException或�?�NullPointerException，就说明格式不对
			return 0;
		}
	}

	/**
	 * <li>功能描述：时间相减得到天�?
	 * 
	 * @param beginDateStr
	 * @param endDateStr
	 * @return long
	 * @author Administrator
	 */
	public static long getDaySub(String beginDateStr, String endDateStr) {
		long day = 0;
		SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
		Date beginDate = null;
		Date endDate = null;
		if (beginDateStr == "" || beginDateStr == null) {
			return 1;
		} else {
			try {
				beginDate = format.parse(beginDateStr);
				endDate = format.parse(endDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
			// System.out.println("相隔的天�?="+day);
			return day;
		}
	}

	/**
	 * 得到n天之后的日期
	 * 
	 * @param days
	 * @return
	 */
	public static String getAfterDayDate(String days) {
		int daysInt = Integer.parseInt(days);

		Calendar canlendar = Calendar.getInstance(); // java.util�?
		canlendar.add(Calendar.DATE, daysInt); // 日期�? 如果不够减会将月变动
		Date date = canlendar.getTime();

		SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = sdfd.format(date);

		return dateStr;
	}

	/**
	 * 得到n天之前的日期
	 * 
	 * @param days
	 * @return
	 */
	public static String getBeforeDayDate(String days) {
		int daysInt = Integer.parseInt(days);
		Calendar canlendar = Calendar.getInstance(); // java.util�?
		canlendar.add(Calendar.DATE, -daysInt); // 日期�? 如果不够减会将月变动
		Date date = canlendar.getTime();
		SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = sdfd.format(date);
		return dateStr;
	}

	/**
	 * 得到n天之后是周几
	 * 
	 * @param days
	 * @return
	 */
	public static String getAfterDayWeek(String days) {
		int daysInt = Integer.parseInt(days);

		Calendar canlendar = Calendar.getInstance(); // java.util�?
		canlendar.add(Calendar.DATE, daysInt); // 日期�? 如果不够减会将月变动
		Date date = canlendar.getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("E");
		String dateStr = sdf.format(date);

		return dateStr;
	}

	/**
	 * 计算当前时间和过去某�?时间的差�?
	 * 
	 * @param oldTime
	 * @return
	 * @throws Exception
	 */
	public static String getDiffTime(String oldTime) throws Exception {
		long minute = 1000 * 60;
		long hour = minute * 60;
		long day = hour * 24;
		long month = day * 30;

		Date dateOld = sdfTime.parse(oldTime);
		long old = dateOld.getTime();
		Date dateNow = new Date();
		long now = dateNow.getTime();
		long diffValue = now - old;

		long monthC = diffValue / month;
		long weekC = diffValue / (7 * day);
		long dayC = diffValue / day;
		long hourC = diffValue / hour;
		long minC = diffValue / minute;

		String time = "刚刚";
		if (monthC >= 1) {
			time = (int) monthC + "个月�?";
		} else if (weekC >= 1) {
			time = (int) weekC + "个星期前";
		} else if (dayC >= 1) {
			time = (int) dayC + "天前";
		} else if (hourC >= 1) {
			time = (int) hourC + "个小时前";
		} else if (minC >= 1) {
			time = (int) minC + "分钟�?";
		} else
			time = "刚刚";

		return time;
	}

	/**
	 * 将当前时间转换为时间�?
	 */
	public static Long dateToLong(String s) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime();
		return ts;
	}

	/**
	 * 将当前时间转换为时间�?
	 */
	public static String dateToStamp(String s) throws ParseException {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime();
		res = String.valueOf(ts);
		return res;
	}

	/**
	 * 将当前时间的00:00:00转换为时间戳
	 */
	public static String dateToStampStart(String s) throws ParseException {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime();
		res = String.valueOf(ts).substring(0, 10);
		return res;
	}

	/**
	 * 将当前时间的00:00:00加上86399000毫秒值差�? 转换为时间戳
	 */
	public static String dateToStampEnd(String s) throws ParseException {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = simpleDateFormat.parse(s);
		long ts = date.getTime() + 86399000;
		res = String.valueOf(ts).substring(0, 10);
		return res;
	}

	/**
	 * 将时间戳转换为时�?
	 */
	public static String stampToDate(String s) {
		String res;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long lt = new Long(s);
		Date date = new Date(lt);
		res = simpleDateFormat.format(date);
		return res;
	}

	/**
	 * 根据出生日期计算年纪 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param birthStr
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int getAgeByBirth(String birthStr) throws Exception {
		int age = 0;
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		Date today = formatDate.parse(DateUtil.getTime());
		Date birth = formatDate.parse(birthStr);
		age = today.getYear() - birth.getYear();
		return age;

	}

	/**
	 * 将yyyy-MM-dd HH:mm:ss格式的转换为yyyy-MM-dd HH:mm格式
	 * 
	 * @param dateStr
	 * @return
	 * @throws Exception
	 */
	public static String modifyDateFormat(String dateStr) throws Exception {
		SimpleDateFormat formatDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date regDate = sdfTime.parse(dateStr);
		return formatDate1.format(regDate);
	}

	/**
	 * 将字符串日期格式�?
	 * 
	 * @throws Exception
	 */
	public static String dateToString(String s) throws Exception {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateFormat.format(simpleDateFormat.parse(s));
	}

	/**
	 * 将yyyy-MM-dd HH:mm 转化�? yyyy-MM-dd HH:mm EEEE
	 * 
	 * @throws ParseException
	 *             2016-10-19 17:00 转化�? 2016-10-19 17:00 星期�?
	 */
	public static String getDate(String date) throws ParseException {
		SimpleDateFormat timer= new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat week = new SimpleDateFormat("EEEE");
		Calendar cal = Calendar.getInstance();
		cal.setTime(timer.parse(date));
		return timer.format(cal.getTime()) + " " + week.format(cal.getTime());

	}

	public static void main(String[] args) {
		// System.out.println(formatStringDate("20161111"));
		System.out.println(getDaySub("2016-11-17 06:01:30", "2016-11-17 16:01:30"));
		// System.out.println(getTime());
		// try {
		// System.out.println(getDate("2016-10-19 17:00"));
		// System.out.println(dateToStamp(getDay()+" 00:00:00"));
		// //System.out.println(dateToStamp(getTime()));
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }
		// System.out.println(getAfterDayWeek("3"));
	}

}
