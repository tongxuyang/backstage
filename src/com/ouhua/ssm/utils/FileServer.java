package com.ouhua.ssm.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;



/**
 * 类名：FileServer.java
 * @author sun
 * @date 创建时间�?2016�?9�?22日下�?1:44:15
 */
public class FileServer {

	public static String writeAddress;
	public static String readAddress;
	
	public enum FileTypeEnum {
		DISH,DISHDETAIL,DISHICON,COURSE,TEACHER,WORDRESUME;
	}
	public static final Integer FILETYPECOMPANYLOGO = 1;
	
	static{
		writeAddress = PropertiesUtils.get("fileServerWriteAddress");
		readAddress = PropertiesUtils.get("fileServerReadAddress");
	}
	public static String processFileSeparator(String filePath){
		if(filePath.contains("\\")){
			return filePath.replaceAll("\\\\", "/");
		}else{
			return filePath;
		}
	}
	
	public static void newFolder(String folderPath) {
		try {
			File ifolder = new File(folderPath);
			if(!ifolder.exists()){
				ifolder.mkdirs();
			}
		} catch (Exception e) {
			System.out.println("新建目录操作出错");
			e.printStackTrace();
		}
	}
	
	public static String getFileName(String filePath){
		String fileName;
		int separatorIndexOfFileName = filePath.lastIndexOf("/");
		if(separatorIndexOfFileName == -1){
			separatorIndexOfFileName = filePath.lastIndexOf("\\");
		}
		fileName =  filePath.substring(separatorIndexOfFileName + 1);
		return fileName;
	}
	public static String getFilePathName(String filePath){
		String fileName;
		int separatorIndexOfFileName = filePath.lastIndexOf("/");
		if(separatorIndexOfFileName == -1){
			separatorIndexOfFileName = filePath.lastIndexOf("\\");
		}
		fileName =  filePath.substring(0, separatorIndexOfFileName + 1);
		return fileName;
	}
	
	public static String getFileSuffix(String fileName){
		return fileName.substring(fileName.lastIndexOf("."));
	}
	
	public static String getRelativePath(String fileName, FileTypeEnum fileType){
		StringBuffer relativePath = new StringBuffer();
		switch (fileType) {
		case DISH:
			relativePath.append(File.separator).append("dish/dish");
			break;
		case DISHDETAIL:
			relativePath.append(File.separator).append("dish/dishdetail");
			break;
		case DISHICON:
			relativePath.append(File.separator).append("dish/dishicon");
			break;
		case COURSE:
			relativePath.append(File.separator).append("course/course");
			break;
		case TEACHER:
			relativePath.append(File.separator).append("course/teacher");
			break;
		default:
			break;
		}
		relativePath.append(File.separator).append(UUID.randomUUID()).append(getFileSuffix(fileName));
		return processFileSeparator(relativePath.toString());
	}
	
	public static String saveFile(File tempFile, FileTypeEnum fileType) throws IOException{
		return saveFile(tempFile.getName(), new FileInputStream(tempFile), fileType);
	}
	/**
	 * 上传文件
	 * @param fileName
	 * @param inputStream
	 * @param fileType
	 * @return
	 * @throws IOException
	 */
	public static String saveFile(String fileName, InputStream inputStream, FileTypeEnum fileType) throws IOException{
		String relativePath = getRelativePath(fileName, fileType);
		String absolutePath = processFileSeparator(writeAddress+relativePath);
		newFolder(getFilePathName(absolutePath));
		File file = new File(absolutePath);
		BufferedInputStream bis = new BufferedInputStream(inputStream);
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
		fileCopy(bis, bos);
		return relativePath;
	}
	public static String saveFile(MultipartFile tempFile, FileTypeEnum fileType) throws IOException{
		return saveFile(tempFile.getOriginalFilename(), tempFile.getInputStream(), fileType);
	}
	
	
	private static void fileCopy (BufferedInputStream bis, BufferedOutputStream bos) throws IOException{
		try {
			byte[] buf = new byte[2048];
			int i;
			while((i = bis.read(buf)) != -1){
				bos.write(buf, 0, i);
			}
			bos.flush();
		} finally{
			bis.close();
			bos.close();
		}
	}
	
}
